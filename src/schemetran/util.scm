;; Copyright 2017 Todor Kondić

;; Licensed under the Apache License, Version 2.0 (the "License");
;; you may not use this file except in compliance with the License.
;; You may obtain a copy of the License at

;;     http://www.apache.org/licenses/LICENSE-2.0

;; Unless required by applicable law or agreed to in writing, software
;; distributed under the License is distributed on an "AS IS" BASIS,
;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;; See the License for the specific language governing permissions and
;; limitations under the License.


;;; Commentary:
;;;
;;;
;;; Some useful procedures.
;;;
;;;
;;; Code:

(define-module (schemetran util)
  #:use-module (ice-9 rdelim)
  #:use-module (ice-9 rw)
  #:use-module (ice-9 format)
  #:use-module (rnrs io ports)
  #:use-module (srfi srfi-1)
  #:export (r+ w+ s+ a+ sp+
	       make-match?-function
	       ident-block
	       non-ident-match?
	       print))
;;; internal helpers
;;; Concat rows
(define-syntax r+
  (syntax-rules ()
    ((_ one)
     (if (list? one)
	 (string-join one "\n")
	 one))
    ((_ one two  ...)
     (string-join `(,one ,two ...) "\n"))))

;;; Concat words
(define-syntax w+
  (syntax-rules ()
    ((_ one)
     (if (list? one)
	 (string-join one " ")
	 one))
    ((_ one two  ...)
     (string-join `(,one ,two ...) " "))))

;;; Concat with no delimiters
(define-syntax s+
  (syntax-rules ()
    ((_ one)
     (if (list? one)
	 (apply string-join one)
	 one))
    ((_ one two  ...)
     (string-append one two ...))))

;;; Concat arguments
(define-syntax a+
  (syntax-rules ()
    ((_ one)
     (if (list? one)
	 (string-join one ", ")
	 one))
    ((_ one two  ...)
     (string-join `(,one ,two ...) ", "))))

;;; Concat with separator
(define-syntax sp+
  (syntax-rules ()
    ((_ sp one)
     (if (list? one)
	 (string-join one sp)
	 one))
    ((_ sp one two ...)
     (string-join  `(,one ,two ...) sp))))

(define-syntax-rule (print x ...)
  (format #f x ...))

(define (make-match?-function first-rxp . rest-rxp)
  (let*
      ((str (fold (lambda (x prev)
		     (s+ "(" x ")|" prev))
		  (s+ "(" first-rxp ")")
		   rest-rxp))
       (patterns (make-regexp str regexp/extended)))
    (lambda (str)
      (if (regexp-exec patterns str) #t #f))))

(define basic-nonident-match? (make-match?-function "\\!OMP\\$" "\\!DIR\\$"))



(define non-ident-match? basic-nonident-match?)




(define (ident-block ident str)
  "Moves all the lines in str by ident spaces, except those which
match with nonident-match?."
  (define blanks (make-string ident #\ ))
  (call-with-output-string (lambda (p-out)
			     (call-with-input-string
				 str
				 (lambda (p-in)
				   
				   (let crawl ((line (read-line p-in)))
				     (if (not (eof-object? line))
					 (begin
					   (if (not (non-ident-match? line))
					       (write-line (string-append blanks line) p-out)
					       (write-line line p-out))
					   (crawl (read-line p-in)))
					 #t)))))))
