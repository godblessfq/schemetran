;; Copyright 2017 Todor Kondić

;; Licensed under the Apache License, Version 2.0 (the "License");
;; you may not use this file except in compliance with the License.
;; You may obtain a copy of the License at

;;     http://www.apache.org/licenses/LICENSE-2.0

;; Unless required by applicable law or agreed to in writing, software
;; distributed under the License is distributed on an "AS IS" BASIS,
;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;; See the License for the specific language governing permissions and
;; limitations under the License.


;;; Commentary:
;;;
;;;
;;; The schemetran module defines basic terms that express Fortran
;;; language as Scheme constructs. The names of most of them resemble
;;; those used while programing in Fortran. For example, `sub' is
;;; procedure used for writing out Fortran subroutines.
;;;
;;; You can use these terms as you would use them when writing in
;;; Fortran. Once you define your subroutines (`sub'), functions
;;; (`fun') and variables (`var'), you can wrap those in Fortan
;;; modules (`mod'), or programs (`prog'). Once the Scheme script
;;; is executed, it will produce a Fortran program.
;;;
;;; The basic building block of a schemetran script is a
;;; string. Fortran part of the schemetran world is represented just
;;; with ordinary strings, so the end product of schemetran terms are
;;; strings. They are just convenience wrappers that help with writing
;;; structured code that is easily changeable depending on
;;; parameters. So, in essence, a typical script will contain a
;;; mixture of terms such as `sub' and `var' and strings that contain
;;; the guts of the program.
;;;
;;; From the outset, the intention of the (original) creator(s) of
;;; schemetran was to produce human-readable code. This is why most
;;; block-like structures, such as `sub', are already indenting the
;;; containing code. If there is a need to indent a generic piece of
;;; code, a convenience schemetran term for this is `ident' which
;;; indents a string, or a list of strings it contains.
;;;
;;; Some useful shortcuts which are not fortran terms are
;;; `s+',`w+',`a+' and `r+' . To join strings, or a list of strings,
;;; use `s+'. To do the same using a blank space as a separator
;;; between them, use `w+'. The `a+' shortcut combines separate
;;; strings into an argument list (strings are separated by
;;; commas). Finally, `r+' combines strings into a larger one in which
;;; the original ones are separated by newlines. It is useful to
;;; combine separate lines, or blocks of code into a single string.
;;;
;;; Primitive fortran types, such as `integer', or `character' are
;;; represented in schemetran as `int' and `char'. The full list of
;;; them can be found in the `#:export' part of schemetran module
;;; definition.
;;;
;;; This is covering everything you typically need to write a
;;; schemetran script. The schemetran terms themselves are documented
;;; at the point of their definition.

;;; Remark: make-... functions are mostly obsolete. Instead of
;;; make-var, use var, make-sub -> sub. They are publicly available at
;;; the moment only because the aspera code is not yet cleaned from
;;; those old references.
;;;
;;; Code:
;;;

(define-module (schemetran)
  #:use-module (ice-9 rdelim)
  #:use-module (ice-9 rw)
  #:use-module (rnrs io ports)
  #:use-module (rnrs)
  #:use-module (schemetran util)
  #:export (
	    ;; types
	    int char real cplx logi

	    ;; set new default kinds
	    set-default-int-kind!
	    set-default-cplx-kind!
	    set-default-real-kind!
	    
	    ;; variable attributes
	    par cont allc ptr
            ini ino inio vol
	    tgt save opti val async
	    
	    true false
	    ;; utility
	    tran-string
	    scheme->tran
	    tran-let
	    def-ident
	    split-line
	    split-lines
	    ident
	    ;; Fortran language elements
	    var
	    vars
	    sub
	    fun
	    mod
	    prog
	    interface
	    cnstr

	    ;; some more utility functions
	    make-comp-line
	    make-make-line
	    make-makefile
	    assume-aligned-block
	    grandmother->frog
	    grandmothers->frogs

	    ;; obsolete (use discouraged)
	    make-prog
	    make-var
	    make-vars
	    make-arr
	    make-scalar
	    make-par-scalar
	    make-par-array
	    make-par-array-from-valist
	    make-ptr-array
	    make-cptr-array
	    make-allc-array
	    make-sub-raw
	    make-fun-raw
	    make-sub
	    make-fun
	    make-mod
	    make-program))




;;; Constants

(define def-ident 4)



;;; types


(define int "integer")
(define char "character")
(define real "real")
(define cplx "complex")
(define logi "logical")


;;; attributes
(define opti "optional")
(define par  "parameter")
(define cont "contiguous")
(define allc "allocatable")
(define ptr "pointer")
(define tgt "target")
(define ino "intent(out)")
(define ini "intent(in)")
(define inio "intent(inout)")
(define vol "volatile")
(define save "save")
(define val "value")
(define async "asynchronous")
;;; values

(define true ".true.")
(define false ".false.")


(define (set-default-int-kind! new-kind)
  "Change current definition of int to integer(@var{new-kind})"
  (set! int (s+ "integer(" new-kind ")")))

(define (set-default-real-kind! new-kind)
  "Change current definition of real to real(@var{new-kind})"
  (set! real (s+ "real(" new-kind ")")))

(define (set-default-cplx-kind! new-kind)
  "Change current definition of cplx to complex(@var{new-kind})"
  (set! real (s+ "complex(" new-kind ")")))

(define (tran-string x)
  "Converts a scheme string @var{x} to a fortran string (adds quotes).
   (tran-string \"/some/path/to/file\") -> \"'/some/path/to/a/file'\""
  (string-append "\"" x "\""))

(define (scheme->tran x)
  "Takes a scheme literal @var{x} and converts it as required. It will
   recursively apply itself to lists and pairs.
  (scheme->tran 1) -> \"1\"
  (scheme->tran \"string\") -> \"'string'\"
  (scheme->tran 1.2e-5) -> \"1.2e-5\"
  (scheme->tran 'sym) -> \"sym\""
  
  (cond
   ((and (number? x) (not (real? x)))
    (string-append "(" (number->string
			(real-part x))
		   ", " (number->string
			 (imag-part x))
		   ")"))
   ((number? x)
    (number->string x))
   
   ((string? x)
    (tran-string x))
   ((symbol? x)
    (symbol->string x))
   ((list? x)
    (map-in-order scheme->tran x))
   ((pair? x)
    `(,(scheme->tran (car x)) . ,(scheme->tran (cdr x))))
   (#t x)))


;;; let-like structure that automatically replaces the local variables
;;; with theyr (scheme->tran ...) analogues.
(define-syntax tran-let
       (syntax-rules ()
         ((_ ((var val) ...) exp exp* ...)
          (let ((var (scheme->tran val)) ...) exp exp* ...))))


(define* (make-var #:key type (kind #f)  (len #f)  (dim #f)  (attr #f)  name (ndim #f) (val #f))
  "Guts of var procedure."
  (define (kind-len)
    (cond
     ((and len kind)
      (string-append "(kind=" kind ", len=" len ")"))
     (kind
      (string-append "(kind=" kind ")"))
     (len
      (string-append "(len=" len ")"))
     (#t
      "")))
  (define (do-ndim x)
    (if x
	(s+ "(" (a+ x) ")")
	""))
  (define (do-dim x)
    (if x
	(s+ ", dimension(" (a+ x) ")")
	""))
  (define (do-val)
    (if val
	(s+ " = " val)
	""))
  (define (do-attr)
    (if attr
	(s+ ", " (a+ attr))
	""))
  (s+ type
      (kind-len)
      ;; (let
      ;; 	  ((x (do-dim dim)))
      ;; 	(if (not (eq? x ""))
      ;; 	    (string-append ", dimension" x)
      ;; 	    ""))
      (do-attr)
      (do-dim dim)
      " :: "
      name
      (do-ndim ndim)
      (do-val)))

(define-syntax-rule (var type name s ...)
  "Creates a string containing a fortran variable definition. The
argument names are self-explaining except for @attr and @vdim. The
former is a list of par, cont, allc, or #f and the latter is a list of
dimensions that are appended after the name.

  (var real \"numarr\" #:ndim '(\":\") #:attr `(,allc)) -> real, allocatable :: numarr(:)
  (var cmplx \"carr\" #:dim '(\":\") #:attr `(,cont ,ptr)) -> complex, dimension(:), contiguous, pointer :: carr
  (var real \"arg\" #:attr `(,ini)) -> real, intent(in) :: arg "
  (make-var #:name name #:type type s ...))
(define-syntax-rule (vars type (name ...) s ...)
  "Create multiple variable definitions differing only in names."
  (r+
   (var type name s ...)
   ...))
(define-syntax make-vars
  (lambda (x)
    (syntax-case x ()
      ((_ (n n* ...) thing* ...)
       #'(string-join
	  `(,(make-var thing* ... #:name n)
	    ,(make-var thing* ... #:name n*) ...)
	  "\n")))))

(define-syntax make-par-vars
  (lambda (x)
    (syntax-case x ()
      ((_ ((n v) (n* v*) ...) thing* ... )
       #'(string-join
	  `(,(make-var thing* ... #:attr `(,par) #:name n #:val v)
	    ,(make-var thing* ... #:attr `(,par) #:name n* #:val v*) ...)
	  "\n")))))



(define (split-line max-len line)
  "Breaks lines the fortran way for line length of max-len."
  (let ((max-col (- max-len 1)))
    (let crawl ((sofar "")
		 (curline line))
	  (if (> (string-length curline) max-len)
	      (let ((left (substring curline 0 max-col))
		    (right (substring curline max-col)))
		(crawl (string-append sofar left "&\n")
		       right))
	      (string-append sofar curline)))))

(define (split-lines max-len multi-line-str)
  "Given a string that spans several lines, @var{multi-line-str},
break it in lines of @var{max-len} length."
  (call-with-output-string (lambda (p-out)
			     (call-with-input-string
				 multi-line-str
				 (lambda (p-in)
				   
				   (let crawl ((line (read-line p-in)))
				     (if (not (eof-object? line))
					 (begin
					   (display
					    (string-append (split-line max-len line) "\n") p-out)
					   (crawl (read-line p-in)))
					 #t)))))))





(define (ident thing . rest)
  "Takes some arguments, or a list of arguments and turns them into an
indented string. Indentation is defined by the def-ident global
variable."
  (if (not (null? thing))
      (if (list? thing)
	  (apply ident thing)
	  (ident-block def-ident (r+ `(,thing . ,rest))))
      ""))


(define (ident-list ident . lines)
  "Given list, it indents and merges items into a block."
  (if (not (null? lines))
      (if (not (list? (car lines)))
	  (let
	      ((fl (make-string ident #\ )))
	    (r+ (map-in-order (lambda (x) (string-append fl x)) lines)))
	  (apply ident-list (cons ident (car lines))))
      ""))




(define (interface name proc-list)
  "Produce an interface with name @var{name} wrapping a list of
procedure names @var{proc-list}.
  
   (interface \"fun\" '(\"fun_a\" \"fun_b\")) -> interface fun
                                                     module procedure :: fun_a
                                                     module procedure :: fun_b
                                                 end interface fun "
  (r+
   (s+ "interface " name)
   (ident
    (map (lambda (x) (s+ "module procedure :: " x)) proc-list))
   (s+ "end interface " name)))

(define* (sub name #:key (i def-ident) (use-i '()) (use '()) (arg '()) (def '()) (body '()))
  "Produce a subroutine with @var{name} name, including a list of
intrinsic modules @var{use-i}, other modules @var{use}, taking
arguments with names @var{arg} and having a list of strings @var{body}
as the body. For the moment, the arguments should be defined in the
@var{body} part of the subroutine.

  (sub \"example_sub\"
       #:use '(\"some_module\")
       #:arg '(\"a\" \"b\")
       #:body `(,(var real \"a\" #:attr `(,ini))
                ,(var real \"b\" #:attr `(,ino))
                 b=a*a)) -> subroutine example_sub
                                use some_module
                                implicit none
                                real, intent(in) :: a
                                real, intent(out) :: b
                                b=a*a
                            end subroutine example_sub"
  (define (id-prepend pref lst)
    (ident-list i
		(map-in-order (lambda (obj)
				(string-append pref obj))
			      lst)))
  (r+
    (string-append "subroutine " name "(" (a+ arg) ")")
    (id-prepend "use, intrinsic :: " use-i)
    (id-prepend "use " use)
    (ident (cons "implicit none" (append def body)))
    (w+ "end subroutine " name)))



(define* (fun type name #:key (i def-ident) (use-i '()) (use '()) (arg '()) (ndim #f) (def '()) (body '()) (elem #f) (pure #f) (attr #f) (dim #f) (rec #f) (bind-c #f))
  "Produces a function with @var{type} type and name @var{name}. If
elemental @var{elem} should be #t. If pure, @var{pure} should be
#t. If it returns an array-like object, provide the list of dimensions
in @var{ndim}.

   (fun cmplx \"bingo\"
        #:pure #t
        #:ndim '(\"5\")
        #:arg '(\"a\")
        #:body `(,(var cmplx \"a\" #:attr `(,ini) #:ndim '(\"5\"))
                 \"bingo=(0,1)*a\")) -> function bingo
                                            implicit none
                                            complex, intent(in) :: a(5)
                                            bingo=(0,1)*a
                                        end function bingo"
  (define (id-prepend pref lst)
    (ident-list i
		(map-in-order (lambda (obj)
				(s+ pref obj))
			      lst)))
  (define prefix
    (cond
     (elem
      "elemental ")
     (pure
      "pure ")
     (rec
      "recursive ")
     (#t
      "")))
  
  (define suffix (if bind-c " bind(c)" ""))
  (r+
    (s+ prefix "function " name "(" (a+ arg) ")" suffix)
    (id-prepend "use, intrinsic :: " use-i)
    (id-prepend "use " use)
    (ident (append '("implicit none")
		 def
		 `(,(var type name #:attr attr #:ndim ndim #:dim dim))
		 body))
    (w+ "end function " name)))


(define* (mod name #:key (use-i '()) (use '()) (def '()) (contains '()) (access "public") (public '()))
  "Produce a module with name @var{name}, optionally using intrinsic
modules given in list @var{use-i}, normal modules in @var{use}, a
section of definitions coming before the contains part of a module in
list @var{defs} and a list of subprograms in @var{contains}. Global
module access is controlled by @var{access} (public by default) and a
list of public names is given by @var{public}.
  (mod \"example\" #:use-i '(\"iso_c_binding\")
                   #:use '(\"some_other_module\")
                   #:def `(,(var real \"glob\")
                           ,(var logi \"another_glob\"))
                   #:contains `(,some-text-defining-subroutine1
                                ...)) ->
  module example
      use, intrinsic :: iso_c_binding
      use :: some_other_module
      implicit none
      real :: glob
      logical :: another_glob
  contains
      subroutine_1
      ...
  
  end module example"
  
  (define (prepend pref lst)
    (map-in-order (lambda (obj)
		    (s+ pref obj))
		  lst))
  (r+
    (string-append "module " name)
    (ident (prepend "use, intrinsic :: " use-i))
    (ident (prepend "use " use))
    "implicit none"
    (ident (map (lambda (x) (s+ "public :: " x)) public))
    (format #f "~a !Default access for the module is ~a ." access access)
    (ident def)
    "contains"
    (ident contains)
    (w+ "end module " name)))



(define* (prog name #:key (inc '()) (use-i '()) (use '()) (def '()) (contains '()) (body '()))
  "Produce a program. In addition to all the arguments that are
explained under mod, it has @var{inc} that gives a list of include
files. These are included before the beginning statement of a program structure:
   include 'a.f90'
   include 'b.f90'
   program prog
       ...
   end program prog"
  (define (prepend pref lst)
    (map-in-order (lambda (obj)
		    (s+ pref obj))
		  lst))
  (r+
   (r+ (map-in-order (lambda (x) (s+ "include " x)) inc))
   (string-append "program " name)
   (ident (prepend "use, intrinsic :: " use-i))
   (ident (prepend "use " use))
   "implicit none"
   (ident def)
   (ident body)
   "contains"
   (ident contains)
   (w+ "end program " name)))



(define (cnstr header statement . body)
  "Given @var{header} and the first @var{statement}, this will produce
an indented block of @var{body} list of lines between the first line
which is 'header statement' and 'end header'."
  (r+ (w+ header statement)
      (ident body)
      (w+ "end" header)))





;;; Produces multiple definitions of the scalars with the same kind and type.
(define-syntax make-scalar
  (syntax-rules (type kind)
    ((_ (type t) (kind k) name ...)
     (string-join `(,(make-var #:type t #:kind k #:name name) ...) "\n"))
    ((_ (type t) name ...)
     (string-join `(,(make-var #:type t #:name name) ...) "\n"))))


;;; Produces multiple definitions of the scalar parameters with the same kind and type.
(define-syntax make-par-scalar
  (syntax-rules (type kind)
    ((_ (type t) (kind k) (name val) ...)
     (string-join `(,(make-var #:type t #:kind k #:name name #:val val #:attr `(,par)) ...) "\n"))
    ((_ (type t) (name val) ...)
     (string-join `(,(make-var #:type t #:name name #:val val #:attr `(,par)) ...) "\n"))))

;;; Produces multiple definitions of the arrays with the same kind, type and intent.
(define-syntax make-array
  (syntax-rules (type kind dim i/o)
    ((_ (type t) (kind k) (dim d) (i/o intent ...) name ...)
     (string-join `(,(make-var #:type t #:kind k #:name name #:ndim d #:attr `(,intent ...)) ...) "\n"))
    ((_ (type t) (kind k) (dim d) name ...)
     (string-join `(,(make-var #:type t #:kind k #:name name #:ndim d) ...) "\n"))))

;;; Produces multiple definitions of the constant arrays with the same kind, type and intent.
(define-syntax make-par-array
  (syntax-rules (type kind dim)
    ((_ (type t) (kind k) (dim d) (name val) ...)
     (string-join `(,(make-var #:type t #:kind k #:name name #:val val #:ndim d #:attr `(,par)) ...) "\n"))))

(define-syntax make-par-array-from-valist
  (syntax-rules (type kind dim)
    ((_ (type t) (kind k) (dim d) (name val) ...)
     (string-join `(,(make-var #:type t #:kind k #:name name #:val (string-append "[" (string-join val ", ") "]") #:ndim d #:attr `(,par)) ...) "\n"))))

;;; Produces multiple definitions of the array pointers with the same kind, type, dim and intent.
(define-syntax make-ptr-array
  (syntax-rules (type kind dim i/o)
    ((_ (type t) (kind k) (dim d) (i/o intent ...) name ...)
     (string-join `(,(make-var #:type t #:kind k #:name name #:ndim d #:attr `(,ptr ,intent ...)) ...) "\n"))))

;;; Produces multiple definitions of cont. array pointers with the same kind, type, dim and intent.
(define-syntax make-cptr-array
  (syntax-rules (type kind dim i/o)
    ((_ (type t) (kind k) (dim d) (i/o intent ...) name ...)
     (string-join `(,(make-var #:type t #:kind k #:name name #:ndim d #:attr `(,cont ,ptr ,intent ...)) ...) "\n"))))

;;; Produces multiple definitions of the allocatable arrays
;;; with the same kind, type, dim and intent.
(define-syntax make-allc-array
  (syntax-rules (type kind dim i/o)
    ((_ (type t) (kind k) (dim d) (i/o intent ...) name ...)
     (string-join `(,(make-var #:type t #:kind k #:name name #:ndim d #:attr `(,allc ,intent ...)) ...) "\n"))))



(define* (make-comp-line fc #:key (idir #f) (ldir #f) (libs #f))
  "A utility function to create a compilation command with compiler
@var{fc}, an optional list of include paths in @var{idir}, an optional
list of library paths @var{ldir} and an optional list of shared
libraries @var{libs}."
  (let*
      ((idirstr (if idir (string-join idir " -I" 'prefix) ""))
       (ldirstr (if ldir (string-join ldir " -L" 'prefix) ""))
       (rdirstr (if ldir (string-join ldir " -Wl,-rpath," 'prefix) ""))
       (lstr (if libs (string-join libs " -l" 'prefix) "")))
    (lambda* (src #:key (out #f) (opts ""))
      (let ((the-out (if out (string-append " -o " out) "")))
	(string-append fc " " opts " " src " " the-out " " idirstr " " ldirstr " " rdirstr " " lstr )))))

;;; Produce a makefile call
(define* (make-make-line #:key (fc "compiler-not-specified")
			       (name "name-not-specified")
			       (source-files '("source-files-not-specified"))
			       (obj-files '()) (inc-dir '()) (lib-dir '())
			       (libs '()) (fcflags "") (make-opts "")
			       (rundir "."))
  "A utility function to produce a command to call make with the
target executable of name @var{name}, with list of object files
@var{obj-files}, list of include directories @var{inc-dir}, a list of
library directories @var{lib-dir}, a list of shared libraries
@var{libs}, the list of compiler flags @var{fcflags} and a list of
make options @var{make-opts}. The makefile needs to follow a specific
format. This function was useful in the ASPERA code, but may not be
that suitable for a general purpose."
  (let*
      ((idirstr (string-join inc-dir " -I" 'prefix))
       (ldirstr (string-join lib-dir " -L" 'prefix))
       (rdirstr (string-join lib-dir " -Wl,-rpath," 'prefix))
       (files (w+ source-files))
       (objfiles (w+ obj-files))
       (lstr (string-join libs " -l" 'prefix)))

    (format #f "make ~a NAME=~a FC=~a FCFLAGS=\"~a\" INCDIR=\"~a\" LIBDIR=\"~a\" FILES=\"~a\" OBJFILES=\"~a\" LIBS=\"~a\" " make-opts name fc fcflags idirstr (w+ ldirstr rdirstr) files objfiles lstr)))

(define* (make-makefile #:key (fc "compiler-not-specified")
			(name "name-not-specified")
			(source-files '("source-files-not-specified"))
			(obj-files '())
			(inc-dir '())
			(lib-dir '()) (libs '()) (fcflags "") (make-opts "")
			(run-line "") (run-dir ""))
  "A utility function to produce a makefile."
  (let* ((idirstr (string-join inc-dir " -I" 'prefix))
	 (ldirstr (string-join lib-dir " -L" 'prefix))
	 (rdirstr (string-join lib-dir " -Wl,-rpath," 'prefix))
	 (files (w+ source-files))
	 (first-file (car source-files))
	 (objfiles (w+ obj-files))
	 (lstr (string-join libs " -l" 'prefix)))
    (r+
     (print "NAME:=~a" name)
     (print "PROGRAM_SOURCE:=~a" first-file)
     (print "FC:=~a" fc)
     (print "FCFLAGS:=~a" fcflags)
     (print "INCDIR:=~a" idirstr)
     (print "LIBDIR:=~a" (w+ rdirstr ldirstr))
     (print "FILES:=~a" files)
     (print "OBJFILES:=~a" objfiles)
     (print "LIBS:=~a" lstr)
     (print "RUNDIR:=~a" run-dir)
     (print "RUN_LINE:=~a" run-line)
     ""
     ".PHONY: clean compile run all"
     ""
     "all: compile"
     ""
     "compile: $(NAME)"
     ""
     "run: $(NAME)"
     "\tcd $(RUNDIR);$(RUN_LINE)"
     ""
     "$(NAME): $(FILES)"
     "\t$(FC) $(FCFLAGS) $(PROGRAM_SOURCE) -o $(NAME) $(LIBDIR) $(INCDIR) $(OBJFILES) $(LIBS)"
     ""
     
     ""
     "clean:"
     "\trm -f $(NAME) *.mod")))


;;; Helper macro to produce Intel aligned block compiler directives.
(define-syntax-rule (assume-aligned-block-expl alignment a a* ...)
  (r+ (string-append "!DIR$ ASSUME_ALIGNED " a ": " alignment)
      (string-append "!DIR$ ASSUME_ALIGNED " a* ": " alignment) ... "\n"))

;;; Helper function to produce Intel aligned block compiler directives.
(define (assume-aligned-block-fun alignment lst)
  (r+ (map-in-order (lambda (x) (s+ "!DIR$ ASSUME_ALIGNED " x ": " alignment)) lst)))

;;; Easy-to-use wrapper to define assume aligned directives.
(define-syntax assume-aligned-block
  (syntax-rules ()
    ((_ alignment a1 a2 a* ...)
     (assume-aligned-block-expl alignment a1 a2 a* ...))
    ((_ alignment one)
     (assume-aligned-block-fun alignment one))))



;;; Obsolete procedures below. Use discouraged.



(define-syntax make-sub-raw
  (syntax-rules (args use-i use)
    ((_ name
	(args a ...)
	(use-i ui ...)
	(use u ...)
	b ...)
     (make-sub-raw def-ident
	       name
	       (args a ...)
	       (use-i ui ...)
	       (use u ...)
	       b ...))
    ((_ ident 
        name 
	(args a ...)
	(use-i ui ...)
	(use u ...)
	b ...)
     (string-join `(,(string-append "subroutine " name "(" 
				   (string-join `(,a ...) ", ")
				   ")")
		    
		    ,(ident-block ident
				  (string-join `( ,(string-append "use, intrinsic :: " ui) ... ,(string-append "use " u) ... ,b ...) "\n"))
		    
		    ,(string-append "end subroutine " name "\n")) "\n"))))



(define-syntax make-fun-raw
  (syntax-rules (args use-i use type kind len dim)
    ((_ ident
        name 
	(pref p ...)
	(type t)
	(kind k)
	(len l)
	(dim d)
	(args a ...)
	(use-i ui ...)
	(use u ...)
	b ...)
     (string-join `(
		      ,(string-append (string-join `(,p ...) ", ") " function " name "(" 
				      (string-join  `(,a ...) ", ")
				      ")") 
		      
		      ,(ident-block ident
				    (string-join `( ,(string-append "use, intrinsic :: " ui) ...
						    ,(string-append "use " u) ...
						    ,(make-var #:type t #:kind k #:len l #:ndim d #:name name)
						    ,b ...) "\n"))
		      
		      ,(string-append "end function " name "\n")) "\n"))))


(define-syntax make-module-access
  (syntax-rules (use defs)
    ((_ name
	(use-i ui ...)
	(use u ...)
	(defs d ...)
	(access acc ...)
	(public pub ...)
	c ...)
     (make-module def-ident
		  name
		  (use-i ui ...)
		  (use u ...)
		  (defs d ...)
		  (access acc ...)
		  (public pub ...)
		  c ...))

    ((_ ident
        name
	(use-i ui ...)
	(use u ...)
	(defs d ...)
	(access acc ...)
	(public pub ...)
	c ...)
     (string-join `(,(string-append "module " name)
		    
		    ,(ident-block ident
				  (string-join `( ,(string-append "use, intrinsic :: " ui) ...
						  ,(string-append "use " u) ...
						  "implicit none"
						  ,(string-append acc) ...
						  ,d ...)
					       
					       "\n"))
		    
		    ,(ident-block ident
				  (string-join `(,(string-append "public :: " pub) ... ) "\n"))
		    "contains"
		    ,(ident-block ident
				  (string-join `(,c ...) "\n"))
		    
		    ,(string-append "end module " name "\n")) "\n"))))


(define-syntax make-program
  (syntax-rules (include use-i use defs contains)
    ((_ name
	(include i ...)
	(include-l inc-list ...)
	(use-i ui ...)
	(use u ...)
	(use-l use-list ...)
	(defs d ...)
	b1 ...
	(contains c ...))
     (make-program def-ident
		   name
		   (include i ...)
		   (include-l inc-list ...)
		   (use-i ui ...)
		   (use u ...)
		   (use-l use-list ...)
		   (defs d ...)
		   b1 ...
		   (contains c ...)))
    ((_ ident
        name
	(include i ...)
	(include-l inc-list ...)
	(use-i ui ...)
	(use u ...)
	(use-l use-list ...)
	(defs d ...)
	b ...
	(contains c ...))
     (string-join `(,(string-append "include " i) ...
		    ,(r+ (map (lambda (x) (string-append "include " x)) inc-list)) ...
		    ,(string-append "program " name)
		    
		    ,(ident-block ident
				  (string-join `(,(string-append "use, intrinsic :: " ui) ...
						 ,(string-append "use " u) ...
						 ,(r+ (map (lambda (x) (string-append "use " x)) use-list)) ...
						 "implicit none"
						 ,d ...
						 ,b ...) "\n"))
		    "contains"
		    ,(ident-block ident
				  (string-join `(,c ...) "\n"))
		    
		    ,(string-append "end program " name "\n")) "\n"))
    ))









;;; Takes a macro that has a form (macro (key1 ...) (key2 ...) val1
;;; val2 ...) and produces a new macro which takes arguments in form
;;; key1 (...) key2 (...) name1 val1 name2 val2 ... , where ordering
;;; of keys is not important (similar to keyword arguments in define* .
;;; key (val1 ...) -> (key val1, val2, ...  key val -> val no key ->
;;; (key) , or nothing depending on bare-or-not
(define-syntax define-keymacro
  (lambda (x)
    (syntax-case x ()
      ((_ new-macro old-macro (id def-val bare-or-not) ...)
       #'(define-syntax new-macro
	   (lambda (x)
	     (define (id? frid thing)
	       (and (identifier? thing)
		    (free-identifier=? frid thing)))
	     (define (crawl frid default bare lst)
	       (let loop ((rest '())
			  (next lst))
		 (if (not (null? next))
		     (let ((y (car next)))
		       (if (not (id? frid y))
			   (loop (cons y rest)
				 (cdr next))
			   `(,(let ((ss (cadr next)))
				(if (not bare)
				    (if (list? (syntax->datum ss))
					`(,y . ,ss)
					`(,y ,ss))
				    ss)) .  ,(append (reverse rest) (cddr next)))))
		     `( ,(if (not (eq? (syntax->datum default) #:NA))
			     (if (not bare)
				 (if (list? default)
				     `(,frid . ,default)
				     `(,frid ,default))
				 default
				 ;; (datum->syntax x default)
				 )
			     (if (not bare)
				 `(,frid)
				 #f)) .  ,(reverse rest)))))
	     (with-ellipsis ***
			    (syntax-case x ()
			      ((_ thing* ***)
			       (with-syntax ((((a ***) . (b ***))
					      (let loop ((res '())
							 (id-next #'(id ...))
							 (def-next `(,(datum->syntax x def-val) ...))
							 (bare-next '(bare-or-not ...))
							 (food #'(thing* ***)))
						(if (not (null? id-next))
						    (let* ((idd (car id-next))
							   (defval (car def-next))
							   (bare (car bare-next))
							   (x (crawl idd defval bare food))
							   (a (car x))
							   (new-food (cdr x)))
						      (if a
							  (loop
							   (cons a res)
							   (cdr id-next)
							   (cdr def-next)
							   (cdr bare-next)
							   new-food)
							  (loop
							   res
							   (cdr id-next)
							   (cdr def-next)
							   (cdr bare-next)
							   new-food)))
						    `(,(reverse res) . ,food)))))
				 #'(old-macro a *** b ***)))))))))))



(define (grandmother->frog grandmother frog dims)
  "Point a 1D-array pointer @var{frog} to a variable
   @var{grandmother} (in other words, turn a grandmother into a
   frog). Variables @var{grandmother} and @var{frog} can be of
   different types."
  (format #f "call c_f_pointer(c_loc(~a),~a,~a)" grandmother frog dims))

(define* (grandmothers->frogs grandmothers dims #:key (frog-prefix "c"))
  ;; Take a bunch of grandmothers and turn them all into frogs. By
  ;; default, frog names are grandmother names prefixed by c.
  (r+ (map (lambda (x) (grandmother->frog x (s+ frog-prefix x) dims)) grandmothers)))

;;; User friendly maker commands (User friendly? I promise I wasn't
;;; drunk when I wrote this.)



(define-syntax mkprg
  (lambda (x)
    (syntax-case x (include include-l use-i use defs contains)
      ((_ ident
	   name (include i ...) (include-l inc-list ...) (use-i ui  ...)
	   (use u ...) (use-l use-list ...) (defs d ...) (contains c ...) b1 ...)
       #'(make-program ident name (include i ...) (include-l inc-list ...) (use-i ui ...) (use u ...)  (use-l use-list ...) (defs d ...)
		       b1 ... (contains c ...))))))

;;; Program maker
(define-keymacro make-prog mkprg (ident def-ident #t)
  (name #:NA #t) (include #:NA #f) (include-l #:NA #f) (use-i #:NA #f) (use #:NA #f) (use-l #:NA #f) (defs #:NA #f) (contains #:NA #f))


;;; Module maker
(define-keymacro make-mod make-module-access (ident def-ident #t) (name #:NA #t) (use-i #:NA #f) (use #:NA #f) (defs #:NA #f) (access #:NA #f) (public #:NA #f))

;;; Subroutine maker
(define-keymacro make-sub make-sub-raw (ident def-ident #t)
  (name #:NA #t) (args #:NA #f) (use-i #:NA  #f) (use #:NA #f))

;; ;;; Function maker
(define-keymacro make-fun make-fun-raw (ident def-ident #t)
  (name #:NA #t) (pref #:NA #f) (type #:NA #f) (kind #f #f) (len #f #f)
  (dim #f #f) (args #:NA #f) (use-i #:NA #f) (use #:NA #f))
;;; Array maker
(define-keymacro make-arr
  make-array (type #:NA #f) (kind #f #f) (dim #f #f) (i/o #f #f))



